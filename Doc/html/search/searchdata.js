var indexSectionsWithContent =
{
  0: "abcdefghiklmnpqrstuwx",
  1: "abdefimpqrst",
  2: "defhimprst",
  3: "bcdefghilmnprstu",
  4: "acdefgklmnpstwx"
};

var indexSectionNames =
{
  0: "all",
  1: "namespaces",
  2: "files",
  3: "functions",
  4: "variables"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Espaces de nommage",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables"
};

