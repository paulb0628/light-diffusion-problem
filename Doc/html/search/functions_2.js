var searchData=
[
  ['deg_5fpol',['deg_pol',['../namespacedeg__pol.html#af18db95bcc1c1951dfb4a3a6ab8757af',1,'deg_pol']]],
  ['diag_5fpol',['diag_pol',['../namespacediag__pol__diff.html#a84ad61165dff51391921a327487ef936',1,'diag_pol_diff.diag_pol()'],['../namespacediag__pol__diff__bhmie.html#a827c1a345e4fd4857131a3c4795ad7d4',1,'diag_pol_diff_bhmie.diag_pol()'],['../namespacediag__pol__diff__RGB.html#a9f4f92408f3bcf69cc02eec5584f6144',1,'diag_pol_diff_RGB.diag_pol()'],['../namespacediag__pol__mie.html#ac27140bf902862982f8ea8af2093ed22',1,'diag_pol_mie.diag_pol()'],['../namespacediag__pol__mie__RGB.html#a28345070fdd929063fd7dbd6875eb676',1,'diag_pol_mie_RGB.diag_pol()']]],
  ['dicho',['dicho',['../namespacefzero.html#a5aca6ce22895976f3d7979cf9858bfa4',1,'fzero']]],
  ['dicho_5fsec',['dicho_sec',['../namespacefzero.html#a048497145471b9aad6dd74ab021702c8',1,'fzero']]]
];
